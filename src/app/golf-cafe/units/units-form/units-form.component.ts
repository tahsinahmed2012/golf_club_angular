import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { UnitService } from '../../services/unit.service';
import { Unit } from '../../../_model/unit';

@Component({
  selector: 'app-units-form',
  templateUrl: './units-form.component.html',
  styleUrls: ['./units-form.component.scss']
})
export class UnitsFormComponent implements OnInit, OnDestroy {
  private onDestroy$: Subject<void> = new Subject<void>();

  unitForm: FormGroup;
  editUnit: Unit;
  id: number;

  constructor(
    private unitService: UnitService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.editUnit = new Unit();
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      if (this.id) {
        this.unitService
          .show(this.id)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe(
            data => {
              this.editUnit = data;
              this.editFormInit();
            },
            error => {
              console.log(error);
            }
          );
      }
    });
    this.formInit();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  editFormInit() {
    this.unitForm.get('name').setValue(this.editUnit.name);
    this.unitForm
      .get('description')
      .setValue(this.editUnit.description);
  }

  private formInit() {
    this.unitForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      description: new FormControl(null, [Validators.required])
    });
  }

  onSubmit() {
    const unit = new Unit(
      this.unitForm.value['name'],
      this.unitForm.value['description']
    );
    if (this.unitForm.valid) {
      if (this.id) {
        // call the update function
        this.unitService
          .update(this.id, unit)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe(
            response => {
              this.router
                .navigateByUrl('/', { skipLocationChange: true })
                .then(() => this.router.navigate(['/units']));
            },
            error => {
              console.log(error);
            }
          );
      } else {
        this.unitService
          .store(unit)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe(response => {
            this.router
              .navigateByUrl('/', { skipLocationChange: true })
              .then(() => this.router.navigate(['/units']));
          });
      }
    } else {
      Object.keys(this.unitForm.controls).forEach(field => {
        // {1}
        const control = this.unitForm.get(field); // {2}
        control.markAsTouched({ onlySelf: true }); // {3}
      });
    }
  }

  onCancel() {
    this.router
      .navigateByUrl('/', { skipLocationChange: true })
      .then(() => this.router.navigate(['/units']));
  }
}
