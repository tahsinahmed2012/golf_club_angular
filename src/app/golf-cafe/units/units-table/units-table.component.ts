import { Component, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { Paginate } from '../../../_model/paginate';
import { LazyLoadEvent } from 'primeng/api';
import { Subject } from 'rxjs';
import { LoaderService } from '../../../shared/service/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UnitService } from '../../services/unit.service';
import { takeUntil } from 'rxjs/operators';
import { Loader } from '../../../_model/loader';

@Component({
  selector: 'app-units-table',
  templateUrl: './units-table.component.html',
  styleUrls: ['./units-table.component.scss']
})
export class UnitsTableComponent implements OnInit, OnDestroy {
  @ViewChildren('dt')
  table;
  units: Paginate[];
  totalRecords: number;
  from: number;
  to: number;
  cols: any[];
  loading = true;
  oldUnitEvent: LazyLoadEvent;
  unitID: any;
  deleteDisplay = false;
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(
    private unitService: UnitService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' }
    ];
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  loadUnitsLazy(event: LazyLoadEvent) {
    this.oldUnitEvent = event;

    this.loaderService.loaderState
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((state: Loader) => {
        this.loading = state.show;
      });
    this.unitService
      .index(event)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(units => {
        this.totalRecords = units.total;
        this.from = units.from;
        this.to = units.to;
        this.units = units.data;
      });
  }

  editUnit(id: number) {
    this.router.navigate(['/units/', id, 'edit'], {
      relativeTo: this.route
    });
  }

  showDeleteDialog(unitID) {
    this.unitID = unitID;
    this.deleteDisplay = true;
  }

  onDeleteSubmit() {
    this.unitService
      .delete(this.unitID)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(response => {
        console.log(response);
        this.loadUnitsLazy(this.oldUnitEvent);
      });
    this.deleteDisplay = false;
  }

  closeDeleteDialog() {
    this.deleteDisplay = false;
  }
}
