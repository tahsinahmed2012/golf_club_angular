import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GolfCafeRoutingModule } from './golf-cafe-routing.module';
import { UnitsComponent } from './units/units.component';
import { CategoryComponent } from './category/category.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryFormComponent } from './Category/category-form/category-form.component';
import { CategoryTableComponent } from './Category/category-table/category-table.component';
import { UnitsTableComponent } from './Units/units-table/units-table.component';
import { UnitsFormComponent } from './Units/units-form/units-form.component';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  imports: [
    CommonModule,
    GolfCafeRoutingModule,
    CardModule,
    InputTextModule,
    InputTextareaModule,
    ButtonModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule
  ],
  declarations: [
    UnitsComponent,
    CategoryComponent,
    DashboardComponent,
    CategoryFormComponent,
    CategoryTableComponent,
    UnitsTableComponent,
    UnitsFormComponent
  ]
})
export class GolfCafeModule {}
