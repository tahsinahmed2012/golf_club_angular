import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { AuthGuard } from '../_guard/auth.guard';
import { UnitsComponent } from './units/units.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'category',
    component: CategoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'category/:id/edit',
    component: CategoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'units',
    component: UnitsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'units/:id/edit',
    component: UnitsComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GolfCafeRoutingModule { }
