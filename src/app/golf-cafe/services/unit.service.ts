import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Paginate } from '../../_model/paginate';
import { Unit } from '../../_model/unit';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  constructor(private http: HttpClient) { }

  index(event): Observable<Paginate> {
    // @ts-ignore
    let params = new HttpParams()
      .set('page', String(event.first / event.rows + 1))
      .set('per_page', event.rows);
    // .set('global', event.filters.global);
    event.filters.global
      ? (params = params.set('global', event.filters.global.value))
      : (params = params);
    event.sortField
      ? (params = params.set(
      'sort_by',
      event.sortField + '.' + event.sortOrder
      ))
      : (params = params);
    return this.http.get<Paginate>('/unit', { params: params });
  }

  store(unit: Unit) {
    return this.http.post<Unit>('/unit', unit);
  }

  update(id: number, unit: Unit) {
    return this.http.put('/unit/' + id, unit);
  }

  show(id: number): Observable<Unit> {
    return this.http.get<Unit>('/unit/' + id);
  }

  delete(id: number) {
    return this.http.delete<Unit>('/unit/' + id);
  }
}
