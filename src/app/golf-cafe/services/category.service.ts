import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Paginate } from '../../_model/paginate';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Category } from '../../_model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  index(event): Observable<Paginate> {
    // @ts-ignore
    let params = new HttpParams()
      .set('page', String(event.first / event.rows + 1))
      .set('per_page', event.rows);
    // .set('global', event.filters.global);
    event.filters.global
      ? (params = params.set('global', event.filters.global.value))
      : (params = params);
    event.sortField
      ? (params = params.set(
      'sort_by',
      event.sortField + '.' + event.sortOrder
      ))
      : (params = params);
    return this.http.get<Paginate>('/category', { params: params });
  }

  store(category: Category) {
    return this.http.post<Category>('/category', category);
  }

  update(id: number, category: Category) {
    return this.http.put('/category/' + id, category);
  }

  show(id: number): Observable<Category> {
    return this.http.get<Category>('/category/' + id);
  }

  delete(id: number) {
    return this.http.delete<Category>('/category/' + id);
  }
}
