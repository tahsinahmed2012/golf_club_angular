import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Category } from '../../../_model/category';
import { CategoryService } from '../../services/category.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit, OnDestroy {
  private onDestroy$: Subject<void> = new Subject<void>();

  categoryForm: FormGroup;
  editCategory: Category;
  id: number;

  constructor(
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.editCategory = new Category();
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      if (this.id) {
        this.categoryService
          .show(this.id)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe(
            data => {
              this.editCategory = data;
              this.editFormInit();
            },
            error => {
              console.log(error);
            }
          );
      }
    });
    this.formInit();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  editFormInit() {
    this.categoryForm.get('name').setValue(this.editCategory.name);
    this.categoryForm
      .get('description')
      .setValue(this.editCategory.description);
  }

  private formInit() {
    this.categoryForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      description: new FormControl(null, [Validators.required])
    });
  }
  // change the id at the later address
  onSubmit() {
    const category = new Category(
      this.categoryForm.value['name'],
      this.categoryForm.value['description']
    );
    if (this.categoryForm.valid) {
      if (this.id) {
        // call the update function
        this.categoryService
          .update(this.id, category)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe(
            response => {
              this.router
                .navigateByUrl('/', { skipLocationChange: true })
                .then(() => this.router.navigate(['/auth/category']));
            },
            error => {
              console.log(error);
            }
          );
      } else {
        this.categoryService
          .store(category)
          .pipe(takeUntil(this.onDestroy$))
          .subscribe(response => {
            this.router
              .navigateByUrl('/', { skipLocationChange: true })
              .then(() => this.router.navigate(['/auth/category']));
          });
      }
    } else {
      Object.keys(this.categoryForm.controls).forEach(field => {
        // {1}
        const control = this.categoryForm.get(field); // {2}
        control.markAsTouched({ onlySelf: true }); // {3}
      });
    }
  }

  onCancel() {
    this.router
      .navigateByUrl('/', { skipLocationChange: true })
      .then(() => this.router.navigate(['/category']));
  }

}
