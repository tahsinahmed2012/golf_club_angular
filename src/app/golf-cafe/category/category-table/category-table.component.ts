import { Component, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { Subject } from 'rxjs';
import { Paginate } from '../../../_model/paginate';
import { LoaderService } from '../../../shared/service/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../../services/category.service';
import { LazyLoadEvent } from 'primeng/api';
import { takeUntil } from 'rxjs/operators';
import { Loader } from '../../../_model/loader';

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss']
})
export class CategoryTableComponent implements OnInit, OnDestroy {
  @ViewChildren('dt')
  table;
  categories: Paginate[];
  totalRecords: number;
  from: number;
  to: number;
  cols: any[];
  loading = true;
  oldCategoryEvent: LazyLoadEvent;
  categoryID: any;
  deleteDisplay = false;
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(
    private categoryService: CategoryService,
    private loaderService: LoaderService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' }
    ];
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  loadCategoriesLazy(event: LazyLoadEvent) {
    this.oldCategoryEvent = event;

    this.loaderService.loaderState
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((state: Loader) => {
        this.loading = state.show;
      });
    this.categoryService
      .index(event)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(categories => {
        this.totalRecords = categories.total;
        this.from = categories.from;
        this.to = categories.to;
        this.categories = categories.data;
      });
  }

  editCategory(id: number) {
    this.router.navigate(['/category/', id, 'edit'], {
      relativeTo: this.route
    });
  }

  showDeleteDialog(categoryID) {
    this.categoryID = categoryID;
    this.deleteDisplay = true;
  }

  onDeleteSubmit() {
    this.categoryService
      .delete(this.categoryID)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(response => {
        console.log(response);
        this.loadCategoriesLazy(this.oldCategoryEvent);
      });
    this.deleteDisplay = false;
  }

  closeDeleteDialog() {
    this.deleteDisplay = false;
  }
}
