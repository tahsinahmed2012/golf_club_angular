import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { takeUntil } from 'rxjs/operators';
import { ProfileService } from '../../auth/service/profile.service';
import * as moment from 'moment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private onDestroy$: Subject<void> = new Subject<void>();

  isLoggedIn$: Observable<boolean>;
  navbarOpen = false;

  user: any;

  validity: any;

  chartOfAccountRoute: any;

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
  ) {}

  ngOnInit() {
    this.loadProfileData();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
  }

  loadProfileData() {
    // this.user = this.profileService.index();
    // this.validity = this.user;
    // console.log(this.validity);
    this.profileService
      .index()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(data => {
        this.user = data;
        this.validity = -moment().diff(this.user.company.valid_to, 'days');
      });
  }

  onLogout() {
    this.authService
      .logout()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(data => {});
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
