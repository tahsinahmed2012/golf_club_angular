import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(private http: HttpClient) {}

  download(report_type, report_name, event): Observable<any> {
    let params = new HttpParams();
    if (event) {
      // put all filters here
      if (event.filters) {
        event.filters.global
          ? (params = params.set('global', event.filters.global.value))
          : (params = params);

        event.filters.date_range
          ? (params = params.set('date_range', event.filters.date_range))
          : (params = params);

        event.filters.code
          ? (params = params.set('code', event.filters.code.value))
          : (params = params);

        event.filters.name
          ? (params = params.set('name', event.filters.name.value))
          : (params = params);

        event.filters.phone
          ? (params = params.set('phone', event.filters.phone.value))
          : (params = params);

        event.filters.address
          ? (params = params.set('address', event.filters.address.value))
          : (params = params);

        event.filters.card_number
          ? (params = params.set(
              'card_number',
              event.filters.card_number.value
            ))
          : (params = params);

        event.filters.connection_date
          ? (params = params.set(
              'connection_date',
              event.filters.connection_date
            ))
          : (params = params);

        event.filters.disconnection_date
          ? (params = params.set(
              'disconnection_date',
              event.filters.disconnection_date
            ))
          : (params = params);

        event.filters.internet
          ? (params = params.set('internet', event.filters.internet.value))
          : (params = params);

        event.filters.id
          ? (params = params.set('id', event.filters.id.value))
          : (params = params);

        event.filters.area
          ? (params = params.set('area', event.filters.area.value))
          : (params = params);

        event.filters.date
          ? (params = params.set('date', event.filters.date))
          : (params = params);

        event.filters.collector
          ? (params = params.set('collector', event.filters.collector.value))
          : (params = params);

        event.filters.subscription_type
          ? (params = params.set(
              'subscription_type',
              event.filters.subscription_type.value
            ))
          : (params = params);

        event.filters.status
          ? (params = params.set('status', event.filters.status.value))
          : (params = params);

        event.filters.fee_type
          ? (params = params.set('fee_type', event.filters.fee_type.value))
          : (params = params);

        event.filters.month
          ? (params = params.set('month', event.filters.month))
          : (params = params);
      }

      event.sortField
        ? (params = params.set(
            'sort_by',
            event.sortField + '.' + event.sortOrder
          ))
        : (params = params);
    }
    const url =
      '/report/download?report_type=' +
      report_type +
      '&report_name=' +
      report_name;
    return this.http.get(url, {
      params: params,
      responseType: 'blob'
    });
  }
}
